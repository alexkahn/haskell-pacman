module Serialize (serialize) where

import qualified Data.Char
import Deserialize

toLower :: String -> String
toLower = map Data.Char.toLower

serializeLives :: Int -> String -> String
serializeLives 0 outputStr = outputStr ++ "\n"
serializeLives x outputStr = serializeLives (x - 1) (outputStr ++ "PacLife ")

serializeMaze :: [[Char]] -> [GameGhost] -> Coordinates -> Float -> String -> String
serializeMaze [] _ _ _ outputStr = outputStr
serializeMaze (row:rows) ghosts pacman y outputStr = serializeMaze rows ghosts pacman (y+1) newOutputStr
  where newOutputStr = serializeRow row ghosts pacman (0,y) outputStr

serializeRow :: [Char] -> [GameGhost] -> Coordinates -> Coordinates -> String -> String
serializeRow [] _ _ _ outputStr = outputStr ++ "\n"
serializeRow ('X':xs) ghosts pacman (x,y) outputStr = serializeRow xs ghosts pacman (x+1,y) $ outputStr ++ "Wall "
serializeRow (cell:cells) ghosts pacman (x,y) outputStr 
  | pacman == (x,y) = serializeRow cells ghosts (x,y) (x+1,y) $ outputStr ++ "Pacman north "
  | blinky = serializeRow cells ghosts pacman (x+1,y) $ outputStr ++ "Ghost blinky " ++  (serializeDot cell) 
  | pinky = serializeRow cells ghosts pacman (x+1,y) $ outputStr ++ "Ghost pinky " ++  (serializeDot cell)
  | inky = serializeRow cells ghosts pacman (x+1,y) $ outputStr ++ "Ghost inky " ++  (serializeDot cell)
  | clyde = serializeRow cells ghosts pacman (x+1,y) $ outputStr ++ "Ghost clyde " ++  (serializeDot cell)
  | otherwise = serializeRow cells ghosts pacman (x+1,y) $ outputStr ++ "Free " ++  (serializeDot cell)
  where 
    blinky = ghostlocation Blinky ghosts (x,y)
    pinky = ghostlocation Pinky ghosts (x,y)
    inky = ghostlocation Inky ghosts (x,y)
    clyde = ghostlocation Clyde ghosts (x,y)
  
serializeDot :: Char -> String
serializeDot '.' = "present "
serializeDot ' ' = "absent "

ghostlocation :: Name -> [GameGhost] -> Coordinates -> Bool
ghostlocation Blinky ghosts coord = ghostCoord == coord
  where GameGhost _ ghostCoord = ghosts !! 0
ghostlocation Pinky ghosts coord = ghostCoord == coord
  where GameGhost _ ghostCoord = ghosts !! 1
ghostlocation Inky ghosts coord = ghostCoord == coord
  where GameGhost _ ghostCoord = ghosts !! 2
ghostlocation Clyde ghosts coord = ghostCoord == coord
  where GameGhost _ ghostCoord = ghosts !! 3

serialize :: Game -> String
serialize game = livesString ++ mazeString
  where 
    lives = remainingLives game
    livesString = serializeLives lives ""
    mze = maze game
    ghostList = ghosts game
    pacman = pacmanCoord game
    mazeString = serializeMaze mze ghostList pacman 0 ""