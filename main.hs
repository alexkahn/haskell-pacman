import Graphics.Gloss
import qualified System.Environment
import Graphics.Gloss.Interface.IO.Game
import System.IO

import qualified Control.Monad
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Foldable
import Deserialize
import Serialize

import qualified Control.Concurrent.STM
import qualified Control.Concurrent.STM.TVar
import qualified Control.Concurrent

import qualified System.Environment

data PacDot = Present | Absent

next :: Direction -> Direction
next West = North
next x = succ x

nextName :: Name -> Name
nextName Clyde = Blinky
nextName x = succ x



width, height, tilesize :: Int
tilesize = 20
tiles = 20*28
width = tilesize*27
height = tilesize*21



main = do [path] <- System.Environment.getArgs
          file <- readFile path
          game <- deserialize file
          playIO window
               background
               1
               game
               draw
               handleEvent
               stepWorld

getCellLocked :: Game -> Coordinates -> Control.Concurrent.STM.TMVar Coordinates
getCellLocked game (x,y)= locks !! (round x + round y * width)
  where locks = mazeLocks game
        width = mazewidth game


moveGhosts :: Game -> [GameGhost] -> IO Game -- Do the forking here
moveGhosts game ghosts = do tvars <- Control.Monad.replicateM (length ghosts) (Control.Concurrent.STM.newEmptyTMVarIO)
                            let cells = map (\(GameGhost _ coordinates) -> getCellLocked game coordinates) ghosts
                            mapM_ (Control.Concurrent.forkIO . (\ghost -> Control.Concurrent.STM.atomically (moveGhost game ghost))) (zip ghosts tvars)
                            coordlist <- mapM (\tmvar -> Control.Concurrent.STM.atomically (Control.Concurrent.STM.takeTMVar tmvar)) tvars
                            let ghostlist = map (\(GameGhost name _, coord)-> GameGhost name coord) (zip ghosts coordlist)
                            Control.Concurrent.STM.atomically $ returnAllLocks coordlist game
                            let inky = newDir (ghosts!! 3) (coordlist !! 3) (inkyDir game)
                            let gme = game {ghosts = ghostlist,
                                            inkyDir = inky}
                            ghostcollisions gme

newDir :: GameGhost -> Coordinates -> Direction -> Direction
newDir (GameGhost _ (x, y)) (nwX, nwY) dir
  | x > nwX = East
  | x < nwX = West
  | y < nwY = South
  | y > nwY = North
  | otherwise = next dir

ghostcollisions :: Game -> IO Game -- Checks also need to happen when pacman is not moving
ghostcollisions game
  | safe == Ghost && remLives > 0 = return game {remainingLives = remLives,
                                             pacmanCoord = origCoord game}
  | safe == Ghost = return game {gamestate = Lost}
  | otherwise = return game
  where remLives = remainingLives game - 1
        coordinates = pacmanCoord game
        safe = noWall game coordinates Nothing

-- moveGhost :: Game -> (GameGhost, Control.Concurrent.STM.TMVar (Float, Float))  -> Control.Concurrent.STM.STM (Float, Float)
moveGhost :: Game -> (GameGhost, Control.Concurrent.STM.TMVar Coordinates)  -> Control.Concurrent.STM.STM ()
moveGhost game ((GameGhost name coord), tmvar) = move (GameGhost name coord) game tmvar


extractLock :: Maybe () -> Coordinates -> Coordinates -> Bool -> Coordinates -- Bool to catch the safe attribute
extractLock Nothing current _ _ = current
extractLock _ _ next True = next
extractLock _ current _ False = current

move :: GameGhost -> Game -> Control.Concurrent.STM.TMVar Coordinates -> Control.Concurrent.STM.STM ()
move (GameGhost Blinky coord) game tmvar  = moveBlinky game coord tmvar  --cell
move (GameGhost Pinky coord) game tmvar = movePinky game coord tmvar
move (GameGhost Inky coord) game tmvar = moveInky game coord tmvar-- cell
move (GameGhost Clyde coord) game tmvar = moveClyde game coord tmvar --cell

moveBlinky :: Game -> Coordinates -> Control.Concurrent.STM.TMVar Coordinates -> Control.Concurrent.STM.STM ()
moveBlinky game (x,y) tmvar = do
        let mze = maze game
        let end = pacmanCoord game
        let (x',y') = findShortest mze (x,y) end
        let next = (fromIntegral x', fromIntegral y')
        let cell = getCellLocked game next
        lock <- Control.Concurrent.STM.tryTakeTMVar cell
        if lock /= Nothing
          then Control.Concurrent.STM.putTMVar tmvar next
          else Control.Concurrent.STM.putTMVar tmvar (x,y)
movePinky :: Game -> Coordinates -> Control.Concurrent.STM.TMVar Coordinates -> Control.Concurrent.STM.STM () -- Always try to match pacman
movePinky game coord tmvar = goSafely game Pinky coord direction tmvar 0
  where
    direction = directionOfPacman game
  -- Works now, changed some names of variables
moveInky :: Game -> Coordinates -> Control.Concurrent.STM.TMVar Coordinates -> Control.Concurrent.STM.STM () -- Always try north
moveInky game coord tmvar = goSafely game Inky coord direction tmvar 0
  where
    direction = inkyDir game
moveClyde :: Game -> Coordinates -> Control.Concurrent.STM.TMVar Coordinates -> Control.Concurrent.STM.STM () -- Always try south
moveClyde game coord tmvar = goSafely game Clyde coord direction tmvar 0
  where
    direction = next $ next $ directionOfPacman game

goSafely :: Game -> Name -> Coordinates -> Direction -> Control.Concurrent.STM.TMVar Coordinates -> Int -> Control.Concurrent.STM.STM () -- Check here if no TVar has the new coordinate
goSafely game ghost coord dir tmvar cntr = do let nxt = go dir coord
                                              let cellstate = (noWall game nxt (Just ghost))
                                              let safe = Dot == cellstate || Empty == cellstate
                                                 -- Should not go on a cell where a ghost is. We don't know if it will move
                                              let cell = getCellLocked game nxt
                                              lock <- Control.Concurrent.STM.tryTakeTMVar cell
                                              if (safe && lock /= Nothing)
                                                then Control.Concurrent.STM.putTMVar tmvar nxt
                                                else if (cntr > 3)
                                                        then Control.Concurrent.STM.putTMVar tmvar coord -- We don't want infinite loops
                                                        else goSafely game ghost coord (next dir) tmvar (cntr + 1)

returnAllLocks :: [Coordinates] -> Game ->  Control.Concurrent.STM.STM [Bool]
returnAllLocks coordlist game = do let cells = map (\coordinates -> getCellLocked game coordinates) coordlist
                                   mapM (\cell -> Control.Concurrent.STM.tryPutTMVar cell (0,0)) cells


window :: Display
window =
    InWindow "Pacman" (width, height) (100,100)

background :: Color
background = black

solidSquare :: Float -> Picture
solidSquare size =
    rectangleSolid size size

coordToLocation :: Coordinates -> Coordinates
coordToLocation (x,y) = (x* tilesizeFloat + tilesizeFloat / 2 - (fromIntegral width/2), (fromIntegral height/2) - y * tilesizeFloat - tilesizeFloat/ 2)
    where tilesizeFloat = fromIntegral tilesize

drawTile :: Char -> Coordinates -> Picture
drawTile char (x,y)
    | char == 'X' = translate (x') (y') $ color (light (light blue)) $ solidSquare tilesizeFloat
    | char == '.' = translate (x') (y') $ color white $ circleSolid 2
    | char == ' ' = translate (x') (y') $ color background $ solidSquare tilesizeFloat
    | otherwise = blank
  where (x', y') = coordToLocation (x, y)
        tilesizeFloat = fromIntegral tilesize
draw :: Game -> IO Picture
draw game = return $ pictures [drawMaze game,
                      drawSprite Paccy (pacmanCoord game) game,
                      drawGhost ((ghosts game) !! 0) game,
                      drawGhost ((ghosts game) !! 1) game,
                      drawGhost ((ghosts game) !! 2) game,
                      drawGhost ((ghosts game) !! 3) game
                     ]

drawSprite :: Name -> Coordinates -> Game -> Picture
drawSprite name (x,y) game = translate x' y' img
  where (x',y') = coordToLocation(x,y)
        img = getImg name

drawGhost :: GameGhost -> Game -> Picture
drawGhost (GameGhost name coord) game = drawSprite name coord game

getImg :: Name -> Picture
getImg Paccy = color yellow $ circleSolid 6
getImg Blinky = color blue $ solidSquare 10
getImg Pinky = color (light red) $ solidSquare 10
getImg Inky = color green $ solidSquare 10
getImg Clyde = color red $ solidSquare 10

drawMaze :: Game -> Picture
drawMaze game = drawMazeLines mze 0
  where mze = maze game

drawMazeLines :: [[Char]] -> Int -> Picture
drawMazeLines [] _ = blank
drawMazeLines (x:xs) row = pictures [drawMazeLine x 0 row, drawMazeLines xs (row + 1)]

drawMazeLine :: [Char] -> Int -> Int -> Picture
drawMazeLine [] _ _ = blank
drawMazeLine (x:xs) column row = pictures [ (drawTile x (fromIntegral column, fromIntegral row)),
                                          drawMazeLine xs (column + 1) row ]

stepWorld :: Float -> Game -> IO Game
stepWorld _ game = moveGhosts game ghostlist
  where
    ghostlist = ghosts game

handleEvent :: Event -> Game -> IO Game
handleEvent (EventKey (Char 'q') Down _ _) game = do let str = serialize game
                                                     savefile str
                                                     return game
handleEvent (EventKey (Char 'w') Down _ _) game = update game North
handleEvent (EventKey (Char 'a') Down _ _) game = update game West
handleEvent (EventKey (Char 's') Down _ _) game = update game South
handleEvent (EventKey (Char 'd') Down _ _) game = update game East
handleEvent (EventKey (Char 'r') Down _ _) game = return $ game { maze = (origMaze game),
                                                                  numberOfEatenDots = 0,
                                                                  gamestate = Playing,
                                                                  pacmanCoord = (origCoord game)
                                                                }
handleEvent _ game = return game

update :: Game -> Direction -> IO Game
update game dir
  | safe == Dot && playing && notDone =
      return game {pacmanCoord = newCoordinates,
                   directionOfPacman = dir,
                   maze = eatBreadcrumb mze newCoordinates,
                   numberOfEatenDots = eatenDots + 1}
  | safe == Dot && playing =  return game {pacmanCoord = newCoordinates,
                                        directionOfPacman = dir,
                                        maze = eatBreadcrumb mze newCoordinates,
                                        numberOfEatenDots = eatenDots + 1,
                                        gamestate = Won}
  | safe == Empty && playing = return game {pacmanCoord = newCoordinates,
                                            directionOfPacman = dir}
  | safe == Wall = return game
  | otherwise = return game
  where
    playing = Playing == gamestate game
    (x,y) = pacmanCoord game
    newCoordinates = go dir (x,y)
    mze = maze game
    eatenDots = numberOfEatenDots game
    notDone = eatenDots /= numberOfDots game
    remLives = remainingLives game - 1
    safe = noWall game newCoordinates Nothing


go :: Direction -> Coordinates -> Coordinates
go North (x,y) = (x,y-1)
go East (x,y) = (x+1,y)
go West (x,y) = (x-1,y)
go South (x,y) = (x,y+1)

removeDot :: Int -> [Char] -> [Char]
removeDot idx list = x ++ " " ++ ys
      where (x,_:ys) = splitAt idx list

replace :: Int -> [Char] -> [[Char]] -> [[Char]]
replace idx string list = x ++ string : ys
      where (x,_:ys) = splitAt idx list

eatBreadcrumb :: [[Char]] -> Coordinates -> [[Char]]
eatBreadcrumb maze (x, y) =  replace (round y) sublist maze
      where
        sublist = removeDot (round x)  $ maze !! (round y)

data CellState = Empty | Dot | Wall | Ghost deriving (Enum, Eq, Ord)

noWall :: Game -> Coordinates -> Maybe Name -> CellState
-- 0 No wall, no dot
-- 1 No wall, a dot
-- 2 Wall
-- 3 GameGhost
noWall game (x, y) ghost
        | collision = Ghost
        | ((mze !! round y) !! round x) == head "." = Dot
        | ((mze !! round y) !! round x) == head "X" = Wall
        | otherwise = Empty
  where mze = maze game
        ghostslist = ghosts game
        collision = touchGost ghostslist (x,y) ghost

touchGost :: [GameGhost] -> Coordinates -> Maybe Name -> Bool
touchGost [] _ _ = False
touchGost ((GameGhost _ ghostcoord):xs) coord Nothing
        | ghostcoord == coord = True
        | otherwise = touchGost xs coord Nothing
touchGost ((GameGhost ghost ghostcoord):xs) coord (Just name)
        | ghostcoord == coord && name /= ghost = True
        | otherwise = touchGost xs coord (Just name)

type IntCoordinates = (Int, Int)


data NeighbourQueue = NeighbourQueue (Set IntCoordinates) [PointNode] deriving (Show)
-- We keep all the PointNodes we visited in a Set. We can use this to efficiently check if we already visited this node
-- The ones we still need to visit are put in a list

data PointNode = Leaf | PointNode (Int, Int) PointNode deriving (Show, Eq, Ord)
-- A PointNode is will keep all the previous PointNodes, this is done to efficiently backtrack



findShortest :: [[Char]] -> Coordinates -> Coordinates -> IntCoordinates
findShortest maze (x,y) (px,py) =
  backtrack maze start endPointNode
  where
    start = (round x, round y)
    end = (round px, round py)
    queue = addNeighbours maze North (PointNode start Leaf) (NeighbourQueue Set.empty [])
    endPointNode = mazerunner maze (PointNode start Leaf) end queue
    -- The escape function will first find the starting and ending point of the maze
    -- Then it will add the direct neighbours of the starting point to the NeigbourQueue
    -- Then it will start searching the path
    -- Once the path is found it will backtrack to mark the path


freeCell :: [[Char]] -> IntCoordinates -> Bool
freeCell maze (x, y) = ((maze !! y) !! x) /= head "X"
-- This function will check if their is a wall at that location

printElem :: [[Char]] -> IntCoordinates -> IO()
printElem maze (x, y) = putChar $ ((maze !! y) !! x)
-- Function used purely for debugging. Shows the maze at a coordinate

mazerunner :: [[Char]] -> PointNode -> IntCoordinates -> NeighbourQueue -> Maybe PointNode
mazerunner maze Leaf _ _ = Nothing
mazerunner maze (PointNode (currentX, currentY) prev) end (NeighbourQueue set queue)
  | current == end = Just (PointNode (currentX, currentY) prev) -- Stop condition
  | queue == [] = Nothing -- Should only happen if no viable way
  | otherwise = mazerunner maze nextPointnode end neigbours
  where
    nextPointnode = head queue
    current = (currentX, currentY)
    remainingSet = Set.insert (currentX, currentY) set
    remainingQueue = tail queue
    neighbourQueue = NeighbourQueue remainingSet remainingQueue
    neigbours = addNeighbours maze North (PointNode current prev) neighbourQueue
    -- This function recursively visits a node and checks if we arrived at the end. If their are no neighbours left the maze is unsolvable. Otherwise it will go to the next neighbour.

addNeighbours :: [[Char]] -> Direction -> PointNode -> NeighbourQueue -> NeighbourQueue
addNeighbours maze windDirection pointnode queue
  | windDirection == West = neighbourQueue -- return the pointnodes with the new West
  | otherwise =
      addNeighbours maze nextDirection pointnode neighbourQueue -- call this function with the new WindDirection
  where
    nextDirection = next windDirection
    neighbourQueue = findNeighbour maze windDirection pointnode queue


findNeighbour :: [[Char]] -> Direction -> PointNode -> NeighbourQueue -> NeighbourQueue
findNeighbour maze windDirection (PointNode (x, y) Leaf) (NeighbourQueue set queue)
  | freeCell maze (xNew, yNew) = neighbourQueue
  | otherwise = NeighbourQueue set queue
  where
    (xNew, yNew) = newCoordinates windDirection (x,y)
    neighbourQueue = NeighbourQueue set (queue ++ [(PointNode (xNew, yNew) (PointNode (x, y) Leaf))]) -- Lazy evaluation makes this possible
findNeighbour maze windDirection (PointNode (x, y) (PointNode (xPrev, yPrev) prev)) (NeighbourQueue set queue)
  | safe && notPrevious && notVisited = neighbourQueue
  | otherwise = NeighbourQueue set queue
  where
    (xNew, yNew) = newCoordinates windDirection (x,y)
    notPrevious = (xNew, yNew) /= (xPrev, yPrev)
    notVisited = Set.notMember (xNew, yNew) set
    safe = freeCell maze (xNew, yNew)
    neighbourQueue = NeighbourQueue set (queue ++ [(PointNode (xNew, yNew) (PointNode (x, y) (PointNode (xPrev, yPrev) prev)))])
    -- Adds neighbours to the queue when they are available.

newCoordinates :: Direction -> IntCoordinates -> IntCoordinates
newCoordinates North (x,y) = (x, y - 1)
newCoordinates South (x,y) = (x, y + 1)
newCoordinates East (x,y) = (x + 1, y)
newCoordinates West (x,y) = (x - 1, y)
-- Abstraction for the calculation of the IntCoordinates of the neighbours

backtrack :: [[Char]] -> IntCoordinates -> Maybe PointNode -> IntCoordinates
backtrack _ _ Nothing = (-1,-1)
backtrack maze coord (Just Leaf) = coord
backtrack maze coord (Just (PointNode (x, y) Leaf)) = (x,y)
backtrack maze (startX, startY) (Just (PointNode (x, y) (PointNode (x', y') Leaf))) = (x,y)
backtrack maze (startX, startY) (Just (PointNode (x, y) (PointNode (x', y') prev)))
    | startX == x' && startY == y' = (x,y) -- Stop condition
    | otherwise = backtrack breadcrumbMaze (startX, startY) (Just (PointNode (x', y') prv))
    where
      breadcrumbMaze = maze
      PointNode (x',y') prv = prev

savefile :: String -> IO ()
savefile string = do let filename = "pacman.pac"
                     writeFile filename string
