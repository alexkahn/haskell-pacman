module Deserialize (deserialize, countDots, Game(..), GameGhost(..), GameState(..), Direction(..),Name(..), Coordinates) where
import qualified Parser
import qualified Control.Monad
import qualified System.Environment
import qualified Control.Concurrent.STM

data Lives = Life | NoLife deriving Show
data Rows = Row [Cell] deriving Show
data Dot = Present | Absent deriving Show
data Name = Blinky | Pinky | Inky | Clyde | Paccy deriving (Eq, Enum, Bounded, Show)
data Direction = North | East | South | West deriving (Eq, Enum, Bounded, Show)
data Cell = Wall | Free Dot | Ghost Name Dot | PacMan Direction deriving Show
data ParsedGame = ParsedGame [Lives] [Rows] deriving Show

data GameState = Playing | Won | Lost deriving (Show, Eq)

data GameGhost = GameGhost Name Coordinates deriving Show

type Coordinates = (Float, Float)
data Game = Game {
maze :: [[Char]],
origMaze :: [[Char]],
ghosts :: [GameGhost],
remainingLives :: Int,
pacmanCoord :: Coordinates,
directionOfPacman :: Direction,
origCoord :: Coordinates,
numberOfDots :: Int,
numberOfEatenDots :: Int,
gamestate :: GameState,
mazesize :: Int,
mazewidth :: Int,
mazeLocks :: [Control.Concurrent.STM.TMVar Coordinates],
inkyDir :: Direction
}

keyword :: String -> a -> Parser.Parser a
keyword str t = Parser.keyword str >> return t

char :: Char -> a -> Parser.Parser a
char c t = Parser.char c >> return t

life :: Parser.Parser Lives
life = Parser.oneof [keyword "PacLife" Life]

rows :: Parser.Parser Rows
rows = Parser.char '\n' >> Control.Monad.liftM Row (Parser.many row)

row :: Parser.Parser Cell
row = Parser.oneof [keyword "Wall" Wall,  ghost, free, pacman]

ghost :: Parser.Parser Cell
ghost = Parser.keyword "Ghost" >> Control.Monad.liftM2 Ghost name dot

free :: Parser.Parser Cell
free = Parser.keyword "Free" >> Control.Monad.liftM Free dot

pacman :: Parser.Parser Cell
pacman = Parser.keyword "Pacman" >> Control.Monad.liftM PacMan direction

name :: Parser.Parser Name
name = Parser.oneof [keyword "blinky" Blinky,keyword "inky" Inky, keyword "pinky" Pinky, keyword "clyde" Clyde]

dot :: Parser.Parser Dot
dot = Parser.oneof [keyword "present" Present, keyword "absent" Absent]

direction :: Parser.Parser Direction
direction = Parser.oneof [keyword "north" North, keyword "south" South, keyword "east" East, keyword "west" West]

game :: Parser.Parser ParsedGame
game = Control.Monad.liftM2 ParsedGame (Parser.many life) (Parser.many rows)

deserialize :: String -> IO Game
deserialize str = do let [(parsedgame, string)] = Parser.apply game str
                     let ParsedGame lives rows = parsedgame
                     let remainingLives = length lives
                     let MazeAndCoordinates maze pacmanCoord dir ghosts = rowsToMaze rows [] (0,0) [GameGhost Blinky (0,0), GameGhost Pinky (0,0), GameGhost Inky (0,0), GameGhost Clyde (0,0)] 0 North
                     let nmbrDots = countDots maze 0
                     let width = (length (head maze))
                     let mzesize = (length maze) * width
                     lockedCells <- Control.Monad.replicateM mzesize $ (Control.Concurrent.STM.newTMVarIO (0,0))
                     return $ Game maze maze ghosts remainingLives pacmanCoord dir pacmanCoord nmbrDots 0 Playing mzesize width lockedCells North

data MazeAndCoordinates = MazeAndCoordinates [[Char]] Coordinates Direction [GameGhost]
rowsToMaze :: [Rows] -> [[Char]] ->  Coordinates -> [GameGhost] -> Float -> Direction -> MazeAndCoordinates
rowsToMaze (Row []:[]) maze coord coordList _ dir = MazeAndCoordinates maze coord dir coordList
rowsToMaze (row:rows) maze coord coordList rowcntr dir = rowsToMaze rows newMaze newCoord newCoordList (rowcntr + 1) direction
  where RowAndCoordinates mazerow newCoord newCoordList _ direction = rowToMaze row [] coord coordList (0, rowcntr) dir
        newMaze = maze ++ [mazerow]

data RowAndCoordinates = RowAndCoordinates [Char] Coordinates [GameGhost] Coordinates Direction
rowToMaze :: Rows -> [Char] -> Coordinates -> [GameGhost] -> Coordinates -> Direction -> RowAndCoordinates
rowToMaze (Row []) row coord coordList coor dir = RowAndCoordinates row coord coordList coor dir
rowToMaze (Row (Wall:cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ "X") coord coordList (x+1,y) dir
rowToMaze (Row ((Free Present):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ ".") coord coordList (x+1,y) dir
rowToMaze (Row ((Free Absent):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ " ") coord coordList (x+1,y) dir
rowToMaze (Row ((Ghost Blinky Present):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ ".") coord (addToGhostList Blinky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Pinky Present):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ ".") coord (addToGhostList Pinky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Inky Present):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ ".") coord (addToGhostList Inky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Clyde Present):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ ".") coord (addToGhostList Clyde (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Blinky Absent):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ " ") coord (addToGhostList Blinky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Pinky Absent):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ " ") coord (addToGhostList Pinky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Inky Absent):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ " ") coord (addToGhostList Inky (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((Ghost Clyde Absent):cells)) row coord coordList (x,y) dir = rowToMaze (Row cells) (row ++ " ") coord (addToGhostList Clyde (x,y) coordList) (x+1,y) dir
rowToMaze (Row ((PacMan direction):cells)) row coord coordList (x,y)  _ = rowToMaze (Row cells) (row ++ " ") (x,y) coordList (x+1,y) direction

addToGhostList :: Name -> Coordinates -> [GameGhost] -> [GameGhost]
addToGhostList Blinky coord ghostlist = [GameGhost Blinky coord] ++ (tail ghostlist)
addToGhostList Pinky coord ghostlist = take 1 ghostlist  ++ [GameGhost Pinky coord] ++  drop 2 ghostlist
addToGhostList Inky coord ghostlist = take 2 ghostlist  ++ [GameGhost Inky coord] ++  drop 3 ghostlist
addToGhostList Clyde coord ghostlist = take 3 ghostlist  ++ [GameGhost Clyde coord]


countDots :: [[Char]] -> Int -> Int
countDots [] cntr = cntr
countDots (m:ms) cntr= countDots ms (cntr + countLine m 0)

countLine :: [Char] -> Int -> Int
countLine [] cntr = cntr
countLine (x:xs) cntr = countLine xs $ cntr + plusdot
  where plusdot = dotChar x

dotChar :: Char -> Int
dotChar '.' = 1
dotChar _ = 0
